Develop React Redux Simple Project by Imgur API's

Implement a simple web app that allows one to browse the Imgur gallery using https://api.imgur.com/

Features:
show gallery images in a grid of thumbnails.
show image description in the thumbnail, top or bottom.
allow selecting the gallery section: hot, top, user.
allow including / excluding viral images from the result set.
allow specifying window and sort parameters.
when clicking an image in the gallery - show its details: big image, title, description, upvotes, downvotes and score.